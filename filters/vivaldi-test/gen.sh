#!/usr/bin/bash
# SPDX-License-Identifier: CC0-1.0

selector=".markdown-heading:first-of-type + ul a"
#declare -a rules

doc_dest="https://github.com/gorhill/uBlock/wiki/Resources-Library"
doc="$(curl -- "$doc_dest")"
scriptlet_name_line="$(<<<"$doc" htmlq -t "$selector")"
<<<"$scriptlet_name_line" readarray -t scriptlet_name

#header="$(<<EOH
#abc
#
#EOH
#cat -
#)"

rm all.usxf
for name in "${scriptlet_name[@]}"
do
	rule="example.com##+js($name)"
	echo "$rule" >> all.usxf
	#rules+=("$rule")
done
#for a in "${rules[@]}"; do echo $a;done

echo "example.com##+js(trusted-rpnt, H1, /./, g)" >> all.usxf

