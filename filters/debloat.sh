#!/usr/bin/bash
# SPDX-License-Identifier: CC0-1.0
# Usage: $ debloater.sh {filter in} {filter out}

file_in="$1"
file_out="$2"

# tests
test -f "$file_in" || (echo "input file doesn't exist"; exit 1)
test -z "$file_out" && (echo "output file is not specified"; exit 1)

# (re)create result file
[ -f "$file_out" ] && rm -vi -- "$file_out"
true > "$file_out"

reComment='^\s*!([^#]|$)'
reEmpty='^\s*$'
firstLine=$(head -n 1 -- "$file_in")

# headers
# if the first line has comment then ~
if [[ $firstLine =~ $reComment ]]
then
	< "$file_in" sed -E '/'"$reComment"'/!q' >> "$file_out"
fi

# rules
#< "$file_in" sed -e '/^$\|^ *!/d' -e 's/^ *//' >> "$file_out"
while read -r l
do
	if [[ $l =~ $reComment ]]
	then continue
	fi

	if [[ $l =~ $reEmpty ]]
	then continue
	fi

	echo "$l" >> "$file_out"

done < "$file_in"

