#!/usr/bin/bash
# SPDX-License-Identifier: CC0-1.0

export TZ=UTC

# config
endpoint="https://api.tweetfeed.live/v1/week/phishing/url"
users=("harugasumi" "KesaGataMe0" "romonlyht"
	"kubotaa3" "catnap707" "NaomiSuzuki_"
	"AP_Zenmashi")
out="./todo-generated.json"

main() {

	# init
	dateLastTweetOld=""
	dateLastTweetNew=""
	jqUsers=""
	jqFilter=""
	jqFromIndex=0
	response=""
	reDateFormat='^[0-9]+-(0[1-9]|1[0-2])-([0-2][0-9]|3[01]) ([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$'

	# prepare
	response="$(curl "$endpoint")"

	if [ -f "$out" ]
	then
		dateLastTweetOld="$(<"./$out" jq -r '.dateLastTweet')"
		jqFromIndex=$(<<<"$response" \
			jq -r '[.[] | .date == "'"$dateLastTweetOld"'"] | index(true)')
	fi

	dateLastTweetNew="$(<<<"$response" jq -r '.[-1].date')"

	# convert a bash array to a jq array lol
	for i in "${users[@]}"
	do
		jqUsers="$jqUsers, \"$i\""
	done
	jqUsers="${jqUsers:2}" # remove unnecessary ", " positioned at head

	# tests
	[ -z "$response" ] && return 1
	[[ ! $dateLastTweetOld =~ $reDateFormat || $jqFromIndex -lt 0 ]] && \
		jqFromIndex=0
	[[ ! $dateLastTweetNew =~ $reDateFormat ]] && \
		(echo "There is neither success nor thing to do."; return 1)
	[ "$dateLastTweetNew" == "$dateLastTweetOld" ] && echo "Nothing to do!"

	# process
	jqFilter='{
	  dateLastTweet: "'"$dateLastTweetNew"'",
	  tweets: [
	    .['"$jqFromIndex:"'][] |
	    select(.user == ('"$jqUsers"')) |
	    {user: .user,date: .date, url: .value, tweet: .tweet}
	  ]
 	}'

	[ -f "./$out" ] && rm  -v -- "$out"
	<<<"$response" jq "$jqFilter" > "$out"
}

main

