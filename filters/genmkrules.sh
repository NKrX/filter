#!/usr/bin/bash
# SPDX-License-Identifier: CC0-1.0

# Usage: $ this.sh {any URL to a document that has #misskey_meta. this need to be valid url form (https://...) and cannot be only the hostname.}
# Example: $ this.sh https://misskey.io/
# NOTE: there is really no error handling except for a dependency check


_depends=("curl" "jq" "htmlq")
endpoint="$1"
meta_selector="#misskey_meta"
meta_imageurls_propchain=".ads[].imageUrl"
reIgnoreUrls=('\?ad=[0-9]{3,5}$' '^https:\/\/random\.misskeyusercontent\.jp\/ads\/' \
	'^https:\/\/res\.zadankai\.club\/ads\/')

main() {
	check_deps || return 1
	genRules
}

check_deps() {
	local i
	local -a _missing_deps

	for i in "${_depends[@]}"
	do command -v "$i" > /dev/null || _missing_deps+=("$i")
	done

	if [ -n "${_missing_deps[*]}" ]
	then
		echo "missing deps to run: ${_missing_deps[*]}"
		return 1
	fi
}

genRules() {
	local doc meta imageurls rule hnOrigin hnImage
	local -a imageurls_array
	local reRuleTemplate='^https?:\/\/(.*)/||\1$image'

	doc="$(curl -s -- "$endpoint")"
	meta="$(<<<"$doc" htmlq -t -- "$meta_selector")"
	imageurls="$(<<<"$meta" jq -rc -- "$meta_imageurls_propchain" -)"
	<<<"$imageurls" readarray -t imageurls_array
	hnOrigin="$(extractHostname "$endpoint")"

	for url in "${imageurls_array[@]}"
	do
		isShouldIgnore "$url" && continue

		hnImage="$(extractHostname "$url")"
		rule=$(<<<"$url" sed -E 's/'"$reRuleTemplate"'/')

		if is3p "$hnOrigin" "$hnImage"
		then rule="${rule},3p"
		else rule="${rule},1p"
		fi
		echo "$rule"
	done
}

isShouldIgnore() {
	local url="$1" re

	for re in "${reIgnoreUrls[@]}"
	do
		if [[ $url =~ $re ]]
		then return 0 # if should
		fi
	done
	return 1
}

extractHostname() {
	local url="$1"
	local reHn='^https:\/\/(([0-9a-zA-Z-]{1,256}\.)*[0-9a-zA-Z-]{1,256})(\/.*|$)'

	<<<"$url" sed -E 's/'"$reHn"'/\1/'
}

is3p() {
	local origin=$1 \
	      target=$2
	local reOrigin="(.*\.|^)${origin}\$"

	if [[ $target =~ $reOrigin ]]
	then return 1
	fi

	# if hn 3p
	return 0
}

main

